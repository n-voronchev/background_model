from collections import namedtuple
import numpy as np
import cv2

from ..filter_base import FilterBase, TFilterInputs, TFilterOutputFrames


__TO_HSV = cv2.COLOR_BGR2HSV


class SimpleTracker(FilterBase):

    def __init__(self, video, motion_mask, mask_contour_min_area=200):
        super().__init__({"video": video, "motion_mask": motion_mask}, ["out_video", "out_bboxes"], input_buffer_min_len=100)  #! 100
        
        self._mask_contour_min_area = mask_contour_min_area

        self._frames_metadata_cache = SimpleCache(self)

    def _do_process_inputs(self, n: int, inputs: TFilterInputs) -> TFilterOutputFrames:
        video = inputs["video"]
        motion_mask = inputs["motion_mask"]

        frame = video.get_frame(n).data
        frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        mask = motion_mask.get_frame(n).data

        cv_contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = [Contour(cv_c) for cv_c in cv_contours]
        mask_spots = list([MaskSpot(c) for c in contours if cv2.contourArea(c) >= self._mask_contour_min_area])
        mask_objects = MaskObjectFactory.from_spots(frame, frame_hsv, mask_spots)

        img = frame  # TODO: make copy if more then 1 receiver on previouse filter
        for o in mask_objects:
            x, y, w, h = o.bbox
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 1)

        

        output_images = self._make_output_images(out=img, out_bboxes=mask_objects)
        return self._output_images_2_output_frames(n, output_images)


class Point(namedtuple('Point', ['x', 'y'])):

    def __init__(self, *args):
        int_args = map(int, args)
        super().__init__(*int_args)


class Rect(namedtuple('Rect', ['x', 'y', 'w', 'h'])):

    def __init__(self, *args):
        super().__init__(*args)
        self.center = Point(self.x + self.w/2, self.y + self.h/2)


class Contour:
    
    def __init__(self, cv_contour):
        self.vertices = cv_contour
        m = cv2.moments(cv_contour)
        self.centroid = Point(m['m10']/m['m00'], m['m01']/m['m00'])
        self.area = cv2.contourArea(cv_contour)


class BboxOwner:

    def __init__(self, bbox_tuple):
        self.bbox = Rect(*bbox_tuple)


class MaskSpot(BboxOwner):

    def __init__(self, contour: Contour):
        self.contour = contour
        
        bbox = cv2.boundingRect(contour.vertices)
        super().__init__(bbox)


class MaskObject(BboxOwner):

    def __init__(self, img_bgr, img_hsv, spots):
        self._image_bgr = img_bgr
        self._image_hsv = img_hsv
        self._spots = spots

        bbox = self._spots[0].bbox
        for s in self._spots[1:]:
            bbox = rect_outline(bbox, s.bbox)
        super().__init__(bbox)

    @property
    def track_point(self) -> Point:
        return self.bbox.center

    '''
    def complare(self, another) -> float:
        """
        return:
            0 - the same
            1 - absolutely different
        """
        x, y, w, h = track_window
        roi = hsv_frame[y:y+h, x:x+w]
    '''

    @classmethod
    def _make_histogram(self, image_hsv):
        hist = cv2.calcHist([image_hsv], [0], None, [180+1], [0, 180+1]) 
        hist = cv2.normalize(hist, hist, 0, 255, cv2.NORM_MINMAX)
        return hist


class MaskObjectFactory:

    NEARBY_CRITERIA = 0.90

    @classmethod
    def from_spots(cls, img_bgr, img_hsv, spots):
        objects = list()

        while len(spots) > 0:
            o = cls(img_bgr, img_hsv, [spots.pop(0)])
            while True:
                spots_idx_to_add = list()
                for s_i, s in enumerate(spots):
                    if o.is_nearby(s):
                        spots_idx_to_add.append(s_i)
                if not spots_idx_to_add:
                    break
                for s_i in sorted(spots_idx_to_add, reverse=True):
                    o._spots.append(spots.pop(s_i))

            o._reinit_fields()
            objects.append(o)

        return objects

    @classmethod
    def is_nearby(cls, spots, new_spot):
        for s in spots:
            centers_dx = abs(s.bbox_center_x - new_spot.bbox_center_x)
            half_sum_w = (s.bbox.w + new_spot.bbox.w) // 2
            
            centers_dy = abs(s.bbox_center_y - new_spot.bbox_center_y)
            half_sum_h = (s.bbox.h + new_spot.bbox.h) // 2
            
            if (centers_dx * cls.NEARBY_CRITERIA <= half_sum_w) and (centers_dy * cls.NEARBY_CRITERIA <= half_sum_h):
                return True
            
        return False


class SimpleCache():

    def __init__(self, filter):
        self._filter = filter
        self._contours = dict()

    def process_mask(self, n, contours):
        self._contours[n] = contours

    def get_contours(self, n):
        return self._contours[n]


def rect_outline(a: Rect, b: Rect) -> Rect:
    x = min(a.x, b.x)
    y = min(a.y, b.y)
    w = max(a.x + a.w, b.x + b.w) - x
    h = max(a.y + a.h, b.y + b.h) - y
    return Rect(x, y, w, h)


def rect_intersec(a: Rect, b: Rect) -> Rect:
    x = max(a.x, b.x)
    y = max(a.y, b.y)
    w = min(a.x + a.w, b.x + b.w) - x
    h = min(a.y + a.h, b.y + b.h) - y
    if w < 0 or h < 0:
        return None
    return Rect(x, y, w, h)


def reсt_area(a: Rect) -> int:
    return a.w * a.h


def get_iou(a: Rect, b: Rect) -> float:
    a_area = reсt_area(a)
    b_area = reсt_area(b)
    
    i_area = reсt_area(rect_intersec(a, b))
    u_area = a_area + b_area - i_area

    iou = i_area / u_area
    return iou


def get_spots_hist(spots):
    hsv = cv.cvtColor(roi,cv.COLOR_BGR2HSV)
    cv.calcHist([hsv],[0, 1], None, [180, 256], [0, 180, 0, 256] )