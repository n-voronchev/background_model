import cv2
import numpy as np

from .filter_base import FilterBase, TFilterInputImages, TFilterOutputImages, TFilterInputs


class MixByMask(FilterBase):
    """
    Output pixels got from:
        img0 -- if (mask == 0)
        img1 -- if (mask != 0)
    """

    def __init__(self, img0, img1, mask):
        super().__init__({"img0": img0, "img1": img1, "mask": mask}, ["out"])

    def _do_init_inputs(self, inputs: TFilterInputs):
        img1 = inputs["img1"]
        if isinstance(img1, np.ndarray):
            self._img1_is_scalar = False
        elif isinstance(img1, (int, float)):
            self._img1_is_scalar = True
        else:
            raise Exception(f"Unsupported 'img1' type: {type(img1)}")

    def _do_process_image(self, n: int, input_images: TFilterInputImages) -> TFilterOutputImages:
        in0 = input_images["img0"]
        in1 = input_images["img1"]
        mask = input_images["mask"]

        """
        if self._invert_mask:
            mask = cv2.bitwise_not(mask)

        if (self._src2 is None) or (self._src2.shape != img_in.shape):
            self._src2 = np.zeros(img_in.shape, dtype=img_in.dtype)

        #img_out = cv2.bitwise_and(img_in, self._src2, mask=mask)
        #img_out = cv2.bitwise_and(img_in, mask)
        """

        # TODO: in0 is number

        out = np.copy(in0)
        
        pixels_from_in1 = (mask != 0)
        if self._img1_is_scalar:
            out[pixels_from_in1] = in1
        else:
            out[pixels_from_in1] = in1[pixels_from_in1]
        
        return self._make_output_images(out=out)
