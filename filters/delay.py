import cv2
import numpy as np

from .filter_base import FilterBase, TFilterInputs, TFilterOutputFrames


class Delay(FilterBase):

    def __init__(self, input, dn=1):
        super().__init__({"in": input}, ["out"])
        self._dn = dn

    def _do_process_inputs(self, n: int, inputs: TFilterInputs) -> TFilterOutputFrames:
        inpt = inputs["in"]

        i_out = inpt.get_frame(n-self._dn).data

        output_images = self._make_output_images(out=i_out)
        return self._output_images_2_output_frames(n, output_images)

