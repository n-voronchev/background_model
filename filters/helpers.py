import numpy as np


def img_float_to_uint8(img_float):
    ii_uint8 = np.iinfo(np.uint8())
    # return img_float.clip(min=ii_uint8.min, max=ii_uint8.max).astype(np.uint8)  --  toooooo sloooooowwwww
    return img_float.astype(np.uint8)
