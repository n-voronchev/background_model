import cv2

from .filter_base import FilterBase, TFilterInputImages, TFilterOutputImages


class GaussianBlur(FilterBase):

    def __init__(self, input, size, sigma=0):
        super().__init__({"in": input}, ["out"])
        self._size = size
        self._sigma = sigma

    def _do_process_image(self, n: int, input_images: TFilterInputImages) -> TFilterOutputImages:
        i_in = input_images["in"]

        i_out = cv2.GaussianBlur(i_in, (self._size, self._size), self._sigma)
        
        return self._make_output_images(out=i_out)
