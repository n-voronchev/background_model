from collections import namedtuple
import threading
import cv2
import sys
from PyQt5.QtWidgets import  QWidget, QLabel, QApplication
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap

from backgrounds import FrameData, IIRFilter, MOG2Filter, KNNFilter, MedianFilter, FrameDiffFilter


class Thread(QThread):

    next_frame = pyqtSignal(object, dict)

    def __init__(self, window, video_src):
        super().__init__(window)
        self.video_fps = 30.0
        self.video_src = video_src

    def run(self):
        cap = cv2.VideoCapture(self.video_src)
        ret, frame = cap.read()
        if not ret:
            print(f'Error: cv2.VideoCapture(\'{self.video_src}\') ret={ret}')
            return False

        iir_acc_time = 3  # seconds
        iir_alpha = 1.0 / (self.video_fps * iir_acc_time)

        iir_filter = IIRFilter(frame, iir_alpha)
        mog2_filter = MOG2Filter(frame, iir_alpha)
        knn_filter = KNNFilter(frame, iir_alpha)
        median_filter = MedianFilter(60, 0)
        framediff_filter_1 = FrameDiffFilter(0, 15, None, 0.1)
        framediff_filter_2 = FrameDiffFilter(25, 15, 5, 0.1)

        while True:
            ret, frame = cap.read()
            if not ret:
                continue

            orig_frame = FrameData('Original video', frame, cv2.COLOR_BGR2RGB)
            iir_bckg = iir_filter.next_frame(frame)
            framediff11_mask, framediff11_bckg = framediff_filter_1.next_frame(frame)
            framediff25_mask, framediff25_bckg = framediff_filter_2.next_frame(frame)
            #mog2_mask, mog2_bckg = mog2_filter.next_frame(frame)
            #knn_mask, knn_bckg = knn_filter.next_frame(frame)
            #mdeian_bckg = median_filter.next_frame(frame)

            frames_data = {
                'orig': orig_frame,
                'iir': iir_bckg,
                'diff_1_m': framediff11_mask,
                'diff_1_f': framediff11_bckg,
                'diff_2_m': framediff25_mask,
                'diff_2_f': framediff25_bckg,
                #'mog2_f': mog2_bckg,
                #'mog2_m': mog2_mask,
                #'knn_f': knn_bckg,
                #'knn_m': knn_mask,
                #'median': mdeian_bckg,
            }

            event = threading.Event()
            self.next_frame.emit(event, frames_data)
            event.wait()  # Wait for UI is updated. The `setText` method is stucking a little bit.


class MainWindow(QWidget):
    
    Size = namedtuple('Size', ('w', 'h'))
    Cell = namedtuple('Cell', ('title_label', 'image_label'))

    columns = 3
    rows = 2
    title_size = Size(640, 20)
    image_size = Size(640, 480)

    def __init__(self, video_src):
        super().__init__()

        self.resize(self.image_size.w * self.columns, (self.image_size.h + self.title_size.h) * self.rows)

        self.cells = [[None for _ in range(self.columns)] for _ in range(self.rows)]
        for i in range(self.rows):
            for j in range(self.columns):
                self.cells[i][j] = self._make_cell(i, j)

        th = Thread(self, video_src)
        th.next_frame.connect(self.draw_next_frame)
        th.start()
        self.show()

    def _make_cell(self, i, j):
        image_label = QLabel(self)
        image_label.resize(*self.image_size)
        image_label.move(self.image_size.w * j, self.image_size.h * i + self.title_size.h * (i + 1))
        title_label = QLabel(self)
        title_label.resize(*self.title_size)
        title_label.move(self.image_size.w * j, (self.image_size.h + self.title_size.h) * i)
        return self.Cell(title_label, image_label)

    @pyqtSlot(object, dict)
    def draw_next_frame(self, done_event, frames_data):
        keys_order = [
            #'orig', 'mog2_m', None, #'knn_m',
            #'iir', 'mog2_f', 'median', #'knn_f',
            'orig', 'diff_1_m', 'diff_2_m',
            'iir', 'diff_1_f', 'diff_2_f',
        ]
        for n, key in enumerate(keys_order):
            if not key:
                continue
            fd = frames_data[key]
            i, j = self._n2ij(n)
            cell = self.cells[i][j]
            cell.image_label.setPixmap(self._frame2pixmap(fd))
            cell.title_label.setText(fd.title)
        done_event.set()

    def _n2ij(self, n):
        i = n // self.columns
        j = n % self.columns
        return (i, j)

    def _frame2pixmap(self, frame_data):
        # https://stackoverflow.com/a/55468544/6622587
        rgb_image = cv2.cvtColor(frame_data.frame, frame_data.color_conv_code)
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        convert_to_qt_format = QImage(rgb_image.data, w, h, bytes_per_line, QImage.Format_RGB888)
        p = convert_to_qt_format.scaled(self.image_size.w, self.image_size.h, Qt.KeepAspectRatio)
        return QPixmap.fromImage(p)

if __name__ == '__main__':
    video_src = sys.argv[1] if len(sys.argv) == 2 else 'axis800x600_oh20.mp4'

    app = QApplication(sys.argv)
    main_window = MainWindow(video_src)
    sys.exit(app.exec_())