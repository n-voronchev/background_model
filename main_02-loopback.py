import sys

import scipy
import numpy as np

from filters import *  #FileReader, Gray, SimpleDiff, BCCorrDiff, InputsDiff, Threshold, MixByMask, GaussianBlur, IIR, Histogram, Max
from application import Application, Column, TAppPipelineReturn


class MyApplication(Application):
    
    def __init__(self, video_src, **kwargs):
        super().__init__(**kwargs)
        self.video_src = video_src

    def pipeline(self) -> TAppPipelineReturn:
        file_reader = FileReader(self.video_src)
        
        src_as_gray = FrameCapture(
            Gray(file_reader.out).out,
            1
        )
        
        blured_gray_src = GaussianBlur(  # Reduce nose in frame-difference
            src_as_gray.out,
            size=15
        )
        
        bc_corr_diff = BCCorrDiff(blured_gray_src.out)
        
        bc_corr_diff_iir = IIR(  # Suppress residual noise in frame-differense (which goes through Gaussian blur)
            bc_corr_diff.out,
            alpha=0.3,
        )

        threshold_framediff = Threshold(
            bc_corr_diff_iir.out,
            #thresh=lambda i: 0.5 * np.mean(i[i > 0]),
            thresh=1, # ???: Magic value 
            erode_size=5,  # ??? 1% from max(w,h)
            erode_iter=3,
            dilate_size=5,
            dilate_iter=4
        )
        threshold_framediff_check = MixByMask(
            img0=src_as_gray.out,
            img1=0,
            mask=threshold_framediff.out
        )
        bckg0 = IIR(  # Just for comperison
            input=src_as_gray.out,
            alpha=0.98,
            mask=threshold_framediff.out,
            invert_mask=True
        )

        bckg1_minus_frame = InputsDiff(
            None,
            src_as_gray.out
        )
        bckg1_minus_frame_thresh = Threshold(
            bckg1_minus_frame.out,
            #thresh=lambda i: 0.5 * np.mean(i[i > 0]),
            thresh=10, # ???: Magic value 
            erode_size=5,  # ??? 1% from max(w,h)
            erode_iter=3,
            dilate_size=5,
            dilate_iter=4
        )
        mask_for_iir_bckg1 = Sum( #Max(
            threshold_framediff.out,
            bckg1_minus_frame_thresh.out,
            eta=1.75
        )
        bckg1 = IIR(
            input=src_as_gray.out,
            alpha=0.98,
            mask=mask_for_iir_bckg1.out,
            mask_type=IIRMaskType.GRAY,
            invert_mask=True,
        )
        bckg1_minus_frame.set_input("in1", Delay(bckg1.out).out)

        outs_to_be_displayed = {
            "file_reader": file_reader.out,
            "blured_gray_src": blured_gray_src.out,
            "bc_corr_diff": bc_corr_diff.out,
            "bc_corr_diff_iir": bc_corr_diff_iir.out,
            "threshold_framediff": threshold_framediff.out,
            "threshold_framediff_check": threshold_framediff_check.out,
            "bckg0": bckg0.out,
            "bckg1_minus_frame": bckg1_minus_frame.out,
            "bckg1_minus_frame_hist": Histogram(bckg1_minus_frame.out).out,
            "bckg1_minus_frame_thresh": bckg1_minus_frame_thresh.out,
            "mask_for_iir_bckg1": mask_for_iir_bckg1.out,
            "bckg1": bckg1.out,
            "bckgs_diff": InputsDiff(bckg0.out, bckg1.out).out,
            "bckg0_err": InputsDiff(bckg0.out, src_as_gray.captured).out,
            "bckg1_err": InputsDiff(bckg1.out, src_as_gray.captured).out,
            "src_as_gray.captured": src_as_gray.captured,
        }

        return outs_to_be_displayed

    def columns(self):
        return [
            Column("source",
                [
                    "file_reader",
                    "blured_gray_src",
                ]),
            Column("thresholded bc_corr_diff",
                [
                    "threshold_framediff",
                    "threshold_framediff_check",
                ]),
            Column("",
                [
                    "mask_for_iir_bckg1",
                    #"bckg1_minus_frame_thresh",
                    #"bckg1_minus_frame",
                    #"bckg1_minus_frame_hist",
                ]),
            Column("backgound and moving objects",
                [
                    "bckg0",
                    "bckg1",
                    "bckgs_diff",
                ]),
            Column("backgound and moving objects",
                [
                    "bckg0_err",
                    "bckg1_err",
                    "src_as_gray.captured"
                ]),
        ]


video_src = sys.argv[1] if len(sys.argv) == 2 else 'axis800x600_oh20.mp4'
app = MyApplication(video_src, w=2300, h=1150)
app.run()