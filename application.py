from collections import namedtuple
import threading
import sys
from typing import List, Dict

import cv2
from PyQt5.QtWidgets import  QWidget, QLabel, QApplication, QScrollArea, QGridLayout
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QImage, QPixmap

from filters import Frame, OutputPort


TAppPipelineReturn = Dict[str, OutputPort]


class Application:

    def __init__(self, w=1800, h=1000):
        self.__w = w
        self.__h = h
    
    def pipeline(self) -> TAppPipelineReturn:
        raise NotImplementedError()

    def columns(self) -> TAppPipelineReturn:
        raise NotImplementedError()

    def run(self):
        qapp = QApplication(list())

        main_window = MainWindow(self)
        main_window.resize(self.__w, self.__h)
        main_window.show()
        
        sys.exit(qapp.exec_())
        

class MainWindow(QScrollArea):

    def __init__(self, app: Application):
        super().__init__()
        main_panel = MainPanel(app)
        self.setWidget(main_panel);

        self._th = VideoThread(main_panel)
        self._th.next_frame.connect(main_panel.draw_next_frame)
        self._th.start()

    def closeEvent(self, event):
        print("UI Thread: closeEvent")
        self._th.requestInterruption()
        self._th.wait(1000)  # ????
        event.accept()


class MainPanel(QWidget):
    
    Size = namedtuple('Size', ('w', 'h'))

    title_size = Size(640, 20)
    image_size = Size(640, 480)

    def __init__(self, app: Application):
        super().__init__()

        self.resize(self.image_size.w * 5.5, (self.image_size.h + self.title_size.h) * 4.5)

        self.columns = app.columns()
        
        pipeline = app.pipeline()
        for col in self.columns:
            col._set_pipeline(pipeline)
        
        layout = QGridLayout(self)
        layout.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        for i, col in enumerate(self.columns):
            layout.addWidget(col, 0, i)

    @pyqtSlot(object, int)
    def draw_next_frame(self, done_event, n: int):
        for col in self.columns:
            col.update_image(n)
        done_event.set()


class VideoThread(QThread):

    next_frame = pyqtSignal(object, int)

    def __init__(self, widget: QWidget):
        super().__init__(widget)

    def run(self):
        n = 0
        while not self.isInterruptionRequested():
            event = threading.Event()
            self.next_frame.emit(event, n)
            event.wait()  # Wait for UI is updated. The `setText` method is stucking a little bit.
            n += 1
        print("VideoThread: loop finished")


class Cell(QWidget):

    def __init__(self, output_name: str, title: str):
        super().__init__()
        self.output_name = output_name

        self.title_label = QLabel()
        self.title_label.resize(*MainPanel.title_size)
        self.title_label.setText(title)
        self.image_label = QLabel()
        self.image_label.resize(*MainPanel.image_size)
        
        layout = QGridLayout(self)
        layout.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        layout.addWidget(self.title_label, 0, 0)
        layout.addWidget(self.image_label, 1, 0)

    def update_image(self, n: int):
        frame = self._filter_out.get_frame(n)
        pixmap = frame2pixmap(frame.data, MainPanel.image_size)
        self.image_label.setPixmap(pixmap)

    def mouseReleaseEvent(self, event):
        self._filter_out.switch_filter()

    def _set_pipeline(self, pipeline: TAppPipelineReturn):
        self._pipeline = pipeline

    @property
    def _filter_out(self):
        return self._pipeline[self.output_name]


class Column(QWidget):

    def __init__(self, title: str, output_names: List[str]):
        super().__init__()
        self.title_label = QLabel()
        self.title_label.resize(*MainPanel.title_size)
        self.title_label.setText(f"_ {title} _")
        self._cells = [Cell(name, name) for name in output_names]
        
        layout = QGridLayout(self)
        layout.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        layout.addWidget(self.title_label, 0, 0)
        for i, cell in enumerate(self._cells):
            layout.addWidget(cell, i+1, 0)

    def update_image(self, n: int):
        for cell in self._cells:
            cell.update_image(n)

    def _set_pipeline(self, pipeline: TAppPipelineReturn):
        for cell in self._cells:
            cell._set_pipeline(pipeline)


def frame2pixmap(frame_data, dest_size):
    # https://stackoverflow.com/a/55468544/6622587
    rgb_image = cv2.cvtColor(frame_data, cv2.COLOR_BGR2RGB)
    h, w, ch = rgb_image.shape
    bytes_per_line = ch * w
    convert_to_qt_format = QImage(rgb_image.data, w, h, bytes_per_line, QImage.Format_RGB888)
    p = convert_to_qt_format.scaled(dest_size.w, dest_size.h, Qt.KeepAspectRatio)
    return QPixmap.fromImage(p)
