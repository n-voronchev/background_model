import cv2
import numpy as np
from .tuples import FrameData

class IIRFilter:

    def __init__(self, init_frame, alpha):
        self._alpha = alpha
        self._background = init_frame.astype(np.float64)
        self._n = 0

    def next_frame(self, in_frame):
        self._n += 1
        self._background = (1.0 - self._alpha) * self._background + self._alpha * in_frame
        out = (
            self._background.astype(np.uint8)
            if (self._n * self._alpha) >= 1.0 else
            np.zeros(in_frame.shape, dtype=np.uint8))
        return FrameData('IIR filter', out, cv2.COLOR_BGR2RGB)
