import sys

import scipy
import numpy as np

from filters import *  #FileReader, Gray, SimpleDiff, BCCorrDiff, InputsDiff, Threshold, MixByMask, GaussianBlur, IIR, Histogram, Max
from application import Application, Column, TAppPipelineReturn

class MyApplication(Application):
    
    def __init__(self, video_src, **kwargs):
        super().__init__(**kwargs)
        self.video_src = video_src

    def pipeline(self) -> TAppPipelineReturn:
        file_reader = FileReader(self.video_src)
        
        src_as_gray = Gray(file_reader.out)
        
        blured_gray_src = GaussianBlur(  # Reduce nose in frame-difference
            src_as_gray.out,
            size=15
        )

        blured_gray_src_iir = IIR(  # Suppress residual noise in frame-differense (which goes through Gaussian blur)
            blured_gray_src.out,
            alpha=0.3,  # 0.3
        )

        lucas_kanade = LucasKanade(
            blured_gray_src_iir.out
        )
        
        farneback = Farneback(
            blured_gray_src_iir.out,
            magnitude_scale_factor=5
        )
        farneback_thresh = Threshold(
            farneback.out_scaled,
            #thresh=lambda i: 0.5 * np.mean(i[i > 0]),
            thresh=1,
            erode_size=5,
            erode_iter=3,
            dilate_size=7,
            dilate_iter=5
        )
        farneback_bckg = IIR(
            input=file_reader.out,
            alpha=0.98,
            mask=farneback_thresh.out,
            mask_type=IIRMaskType.BINARY,
            invert_mask=True,
        )

        my_lucas_kanade = MyLucasKanade(
            blured_gray_src_iir.out,
            sobel_ksize=3,
            lk_ksize=7
        )
        my_lucas_kanade_thresh = Threshold(
            my_lucas_kanade.out,
            thresh=10,
            erode_size=5,
            erode_iter=3,
            dilate_size=7,
            dilate_iter=5
        )
        my_lucas_kanade_bckg = IIR(
            input=file_reader.out,
            alpha=0.98,
            mask=my_lucas_kanade_thresh.out,
            mask_type=IIRMaskType.BINARY,
            invert_mask=True,
        )

        my_horn_schunck = MyHornSchunck(
            blured_gray_src_iir.out,
            magnitude_scale_factor=5
        )
        my_horn_schunck_thresh = Threshold(
            my_horn_schunck.out_scaled,
            thresh=10,
            erode_size=5,
            erode_iter=3,
            dilate_size=7,
            dilate_iter=5
        )
        my_horn_schunck_bckg = IIR(
            input=file_reader.out,
            alpha=0.98,
            mask=my_horn_schunck_thresh.out,
            mask_type=IIRMaskType.BINARY,
            invert_mask=True,
        )
        
        outs_to_be_displayed = {
            "file_reader": file_reader.out,
            "blured_gray_src_iir": blured_gray_src_iir.out,
            "lucas_kanade": lucas_kanade.out,
            "my_lucas_kanade": my_lucas_kanade.out,
            "my_lucas_kanade_thresh": my_lucas_kanade_thresh.out,
            "my_lucas_kanade_bckg": my_lucas_kanade_bckg.out,
            "farneback_thresh": farneback_thresh.out,
            "farneback_normalized": farneback.out_normalized,
            "farneback_bckg": farneback_bckg.out,
            "my_horn_schunck_normalized": my_horn_schunck.out_normalized,
            "my_horn_schunck_thresh": my_horn_schunck_thresh.out,
            "my_horn_schunck_bckg": my_horn_schunck_bckg.out,
        }

        return outs_to_be_displayed

    def columns(self):
        return [
            Column("source",
                [
                    "file_reader",
                    "blured_gray_src_iir",
                ]),
            Column("",
                [
                    "farneback_bckg",
                    "farneback_thresh",
                    "farneback_normalized",
                ]),
        ]
        '''
        Column("",
            [
                "lucas_kanade"
            ]),
        Column("",
            [
                "my_lucas_kanade_bckg",
                "my_lucas_kanade_thresh",
                "my_lucas_kanade",
            ]),
        Column("",
            [
                "my_horn_schunck_bckg",
                "my_horn_schunck_thresh",
                "my_horn_schunck_normalized",
            ]),
        '''

video_src = sys.argv[1] if len(sys.argv) == 2 else 'axis800x600_oh20.mp4'
app = MyApplication(video_src, w=2300, h=1150)
app.run()