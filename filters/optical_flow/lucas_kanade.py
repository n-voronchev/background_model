import cv2
import numpy as np

from ..filter_base import FilterBase, TFilterInputs, TFilterOutputFrames


class LucasKanade(FilterBase):

    def __init__(self, input):
        super().__init__({"input": input}, ["out"], input_buffer_min_len=2)
        
        self.lk_params = {
            "winSize": (15,15),
            "maxLevel": 2,
            "criteria": (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03)
        }

    def _do_init_inputs(self, inputs: TFilterInputs):
        input = inputs["input"]
        l = 50
        self.point_to_track = np.array(
            [ [[j*l, i*l]] for i, j in np.ndindex(*map(lambda x: x//l, input.shape)) ]
        ).astype(np.float32)
        print(f"self.point_to_track[0]  = {self.point_to_track[0]}")
        print(f"self.point_to_track[-1] = {self.point_to_track[-1]}")
        print(f"self.point_to_track     = {input.shape}")


    def _do_process_inputs(self, n: int, inputs: TFilterInputs) -> TFilterOutputFrames:
        inpt = inputs["input"]

        I_at_n = inpt.get_frame(n).data
        I_at_n_1 = inpt.get_frame(n-1).data

        """        
        feature_params = dict(
            maxCorners = 10,    
            qualityLevel = 0.3,
            minDistance = 7)
        p0 = cv2.goodFeaturesToTrack(I_at_n, mask=None, **feature_params)
        """

        #self.point_to_track = np.array([[[100., 100.]]]).astype(np.float32)
        #self.point_to_track = p0
        new_points, st, err = cv2.calcOpticalFlowPyrLK(I_at_n_1, I_at_n, self.point_to_track, None, **self.lk_params)
        new_points = new_points
        #print("my:", (new_points[st==1]).shape)

        i_out = I_at_n.astype(np.uint8)  # Always lays in the interval [0, 255]
        for p in new_points[st==1]:
            x, y = p.ravel()
            i_out = cv2.circle(i_out, (int(x),int(y)), 5, 100, -1)        

        output_images = self._make_output_images(out=i_out)
        return self._output_images_2_output_frames(n, output_images)