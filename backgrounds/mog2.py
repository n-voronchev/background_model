import cv2
from .cv2_bckg_substractor import Cv2BckgSubstractor

class MOG2Filter(Cv2BckgSubstractor):

    def __init__(self, init_frame, iir_alpha):
        super().__init__(cv2.createBackgroundSubtractorMOG2(), 'MOG2', init_frame, iir_alpha)
