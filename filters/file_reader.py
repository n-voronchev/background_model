import cv2

from . import VideoSourceEndError
from .filter_base import FilterBase, TFilterInputImages, TFilterOutputImages, ShapesCollector, Dict


class FileReader(FilterBase):

    def __init__(self, video_src):
        super().__init__(None, ["out"])
        self._video_src = video_src
        self._img0 = self._reopen_and_read()

    def _do_process_image(self, n: int, input_images: TFilterInputImages) -> TFilterOutputImages:
        if n == 0:
            image = self._img0
        else:
            ret, image = self._cap.read()
            if not ret:
                image = self._reopen_and_read()

        N = 100
        if n == N:
            cv2.imwrite(f"frame-{N}.png", image) 
        if n == N+1:
            cv2.imwrite(f"frame-{N+1}.png", image) 

        return self._make_output_images(out=image)

    def _do_get_output_shapes(self, shapes_collector: ShapesCollector) -> Dict[str, tuple]:
        return {"out": tuple(self._img0.shape[:2])}

    def _reopen_and_read(self):
        self._cap = cv2.VideoCapture(self._video_src)
        if not self._cap.isOpened():
            raise VideoSourceEndError(f"Error: cv2.VideoCapture('{self._video_src}')")
        ret, image = self._cap.read()
        if not ret:
            raise VideoSourceEndError(f"Error: ret={repr(ret)} for '{self._video_src}'")
        return image
