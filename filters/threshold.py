import cv2

from .filter_base import FilterBase, TFilterInputImages, TFilterOutputImages


class Threshold(FilterBase):

    def __init__(self, input,
                 thresh=127, maxval=255, type=cv2.THRESH_BINARY,
                 erode_size=None, erode_iter=1,
                 dilate_size=None, dilate_iter=1):
        super().__init__({"in": input}, ["out"])
        self._thresh = thresh
        self._maxval = maxval
        self._type = type

        self._erode_kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (erode_size, erode_size)) if erode_size else None
        self._dilate_kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (dilate_size, dilate_size)) if dilate_size else None
        self._erode_iter = erode_iter
        self._dilate_iter = dilate_iter


    def _do_process_image(self, n: int, input_images: TFilterInputImages) -> TFilterOutputImages:
        i_in = input_images["in"]

        thresh = (
            self._thresh(i_in)
            if callable(self._thresh) else
            self._thresh
        )
        retval, i_out = cv2.threshold(i_in, thresh, self._maxval, self._type)

        if self._erode_kernel is not None:
            i_out = cv2.erode(i_out, self._erode_kernel, iterations=self._erode_iter)
            #i_out = cv2.morphologyEx(i_out, cv2.MORPH_OPEN, self._erode_kernel)

        if self._dilate_kernel is not None:
            i_out = cv2.dilate(i_out, self._dilate_kernel, iterations=self._dilate_iter)
            #i_out = cv2.morphologyEx(i_out, cv2.MORPH_CLOSE, self._dilate_kernel)

        return self._make_output_images(out=i_out)
