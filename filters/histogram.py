import cv2
import numpy as np
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.figure import Figure

from .filter_base import FilterBase, TFilterInputImages, TFilterOutputImages, TFilterOutputFrames


class Histogram(FilterBase):

    def __init__(self, input):
        super().__init__({"in": input}, ["out"])
        
        ii_uint8 = np.iinfo(np.uint8())
        self.__uint8_max = ii_uint8.max

        self._enabled = False

    def _do_process_image(self, n: int, input_images: TFilterInputImages) -> TFilterOutputImages:
        i_in = input_images["in"]

        assert len(i_in.shape) == 2, f"i_in.shape={i_in.shape}"
        assert i_in.dtype == np.uint8, f"i_in.dtype={i_in.dtype}"

        fig, ax = self._make_ax(i_in)

        ax.hist(i_in.reshape(-1), bins=self.__uint8_max+1, range=(0, np.max(i_in)))
        
        i_out = self._fig_to_img(fig)
        return self._make_output_images(out=i_out)

    def _make_ax(self, i_in):
        fig = Figure()
        ax = fig.gca()
        ax.set_ylim(0, i_in.size)
        return fig, ax

    def _fig_to_img(self, fig):
        canvas = FigureCanvasAgg(fig)
        canvas.draw()
        return np.asarray(canvas.buffer_rgba())
