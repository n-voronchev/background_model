from __future__ import annotations  # [PEP 563]
from collections import deque, namedtuple
from typing import Dict, List

import numpy as np

from .frame import Frame, TImage


class BufferError(Exception):
    pass


class OutputPort:

    def __init__(self, filter: FilterBase, name: str):
        self._filter = filter
        self._name = name
        self._shape = None

    @property
    def shape(self):
        return self._get_shape(dict())

    def get_frame(self, n: int) -> Frame:
        frames = self._filter._get_frames_off_all_outputs(n)
        return frames[self._name]

    def switch_filter(self):
        self._filter.switch()

    def extend_buffer(self, min_len):
        self._filter.extend_buffer(min_len)

    def _get_shape(self, shapes_collector: ShapesCollector):
        if self._shape is None:
            shapes = self._filter._get_shape_off_all_outputs(shapes_collector)
            self._shape = shapes[self._name]
        return self._shape


class InputPort:

    def __init__(self, name: str, partner_out: OutputPort):
        self._name = name
        self._partner_out = partner_out

    def get_frame(self, n) -> Frame:
        return self._partner_out.get_frame(n)

    @property
    def shape(self):
        return self._get_shape(dict())

    def _get_shape(self, shapes_collector: ShapesCollector):
        return self._partner_out._get_shape(shapes_collector)


"""
class InputPortWithDelay(InputPort):

    def __init__(self, name: str, partner_out: OutputPort, delay_frames: int):
        super().__init__(name, partner_out)
        assert delay_frames > 0
        self._delay_frames = delay_frames

    def get_frame(self, n) -> Frame:
        n_shifted = n - self._delay_frames
        if n_shifted < 0:
            return 0
        return self._partner_out.get_frame(n_shifted)
"""


class BufferItem(namedtuple("BufferItem", ["n", "output_frames"])):
    
    def __init__(self, n: int, output_frames: TFilterOutputFrames):
        super().__init__()
        if any(f.n != n for f in output_frames.values()):
            raise BufferError()


class FilterBase:

    def __init__(self, inputs: TFilterInputs, output_names: list[str], input_buffer_min_len=1):
        self._inputs = {n: (o and InputPort(n, o)) for n, o in (inputs or dict()).items()}
        self._output_names = output_names
        self._inputs_buffer_min_len = input_buffer_min_len
        self._buffer = deque(maxlen=1)
        self._enabled = True

        for out_name in output_names:
            setattr(self, out_name, OutputPort(self, out_name))

        self._try_init_inputs()

    def switch(self):
        self._enabled = not self._enabled

    def extend_buffer(self, min_len):
        if self._buffer.maxlen < min_len:
            new_buffer = deque(maxlen=min_len)
            new_buffer.extend(self._buffer)
            self._buffer = new_buffer

    '''
    def set_input_with_delay(self, input_name: str, input: OutputPort):
        assert self._inputs.get(input_name, 0) is None
        self._inputs[input_name] = InputPortWithDelay(input_name, input, 1)
        self._try_init_inputs()
    '''

    def set_input(self, input_name: str, input: OutputPort):
        """
        Possible implementations:
            * N-1 frame
            * Конечный цикл для последовательных приближейний (сontraction mapping???)
        """
        assert self._inputs.get(input_name, 0) is None
        self._inputs[input_name] = InputPort(input_name, input)
        self._try_init_inputs()

    def _try_init_inputs(self):
        if None in self._inputs.values():
            return False
        for i in self._inputs.values():
            if isinstance(i, InputPort):
                i._partner_out.extend_buffer(self._inputs_buffer_min_len)
        self._do_init_inputs(self._inputs)
        return True

    def _get_shape_off_all_outputs(self, shapes_collector: ShapesCollector):
        not_found = object()
        cached_shape = shapes_collector.get(self, not_found)
        if cached_shape is not_found:
            shapes_collector[self] = None
            shapes = self._do_get_output_shapes(shapes_collector)
            shapes_collector[self] = shapes
            return shapes
        elif cached_shape is None:
            return None
        return cached_shape

    def _get_frames_off_all_outputs(self, frame_n: int) -> TFilterOutputFrames:
        if frame_n < 0:
            frame_n = 0

        for n in range(self._last_frame_n + 1, frame_n + 1):
            if (self._enabled) and (n != 0):
                output_frames = self._do_process_inputs(n, self._inputs)
            else:
                output_frames = self._do_disabled_filter(n, self._inputs)
            buffer_item = BufferItem(n=n, output_frames=output_frames)
            self._buffer.append(buffer_item)

        i = frame_n - self._first_frame_n
        if i < 0:
            return self._make_output_black_frames(frame_n)

        try:
            return self._buffer[i].output_frames
        except IndexError:
            raise BufferError(f"i={i} while:\n  frame_n={frame_n}\n  first_frame_n={self._first_frame_n}\n  last_frame_n={self._last_frame_n}\n  output_buffer_len={self._buffer.maxlen}")

    def _do_init_inputs(self, inputs: TFilterInputs):
        pass

    def _do_get_output_shapes(self, shapes_collector: ShapesCollector) -> Dict[str, tuple]:
        input_shapes_generator = (i._get_shape(shapes_collector) for i in self._inputs.values())
        first_input_shape = next(filter(None, input_shapes_generator), None)
        return {name: first_input_shape for name in self._output_names}

    def _do_disabled_filter(self, n: int, input_images: TFilterInputImages) -> TFilterOutputFrames:
        return self._make_output_black_frames(n)

    def _do_process_inputs(self, n: int, inputs: TFilterInputs) -> TFilterOutputFrames:
        input_frames = self._inputs_2_input_frames(n, inputs)
        output_frames = self._do_process_frame(n, input_frames)
        return output_frames

    def _do_process_frame(self, n: int, input_frames: TFilterInputFrames) -> TFilterOutputFrames:
        input_images = self._input_frames_2_input_images(input_frames)
        output_images = self._do_process_image(n, input_images)
        output_frames = self._output_images_2_output_frames(n, output_images)
        return output_frames

    def _do_process_image(self, n: int, input_images: TFilterInputImages) -> TFilterOutputImages:
        raise NotImplementedError("You must implement this method in the derived class.")

    def _inputs_2_input_frames(self, n: int, inputs: TFilterInputs) -> TFilterInputFrames:
        return {inpt_name: (inpt and inpt.get_frame(n)) for inpt_name, inpt in inputs.items()}

    def _input_frames_2_input_images(self, input_frames: TFilterInputFrames) -> TFilterInputImages:
        return {inpt_name: (f and f.data) for inpt_name, f in input_frames.items()}

    def _make_output_images(self, **kwargs: TImage) -> TFilterOutputImages:
        if set(kwargs.keys()) ^ set(self._output_names):
            raise Exception(f"Used output port names: {kwargs.keys()}. But is must be {self._output_names}.")
        return kwargs

    def _output_images_2_output_frames(self, n: int, output_images: TFilterOutputImages) -> TFilterOutputFrames:
        return {out_name: Frame(img, n) for out_name, img in output_images.items()}

    def _make_output_black_frames(self, n: int) -> TFilterOutputFrames:
        return {name: Frame.black_frame(n, self._get_output(name).shape) for name in self._output_names}

    @property
    def _last_frame_n(self) -> int:
        return -1 if len(self._buffer) == 0 else self._buffer[-1].n

    @property
    def _first_frame_n(self) -> int:
        return -1 if len(self._buffer) == 0 else self._buffer[0].n

    def _get_output(self, out_name: str) -> OutputPort:
        return getattr(self, out_name)

"""
def is_dummy_black(img):
    if isinstance(img, int):
        if img == 0:
            return True
        else:
            raise Exception(f"Not black: img={img}")
    else:
        return False
"""

TFilterInputs = Dict[str, OutputPort]
TFilterInputImages = Dict[str, TImage]
TFilterInputFrames = Dict[str, Frame]
TFilterOutputImages = Dict[str, TImage]
TFilterOutputFrames = Dict[str, Frame]
ShapesCollector = Dict[FilterBase, tuple]