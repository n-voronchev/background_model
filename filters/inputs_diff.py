import cv2

from .filter_base import FilterBase, TFilterInputImages, TFilterOutputImages


class InputsDiff(FilterBase):

    def __init__(self, in1, in2):
        super().__init__({"in1": in1, "in2": in2}, ["out"])

    def _do_process_image(self, n: int, input_images: TFilterInputImages) -> TFilterOutputImages:
        i1, i2 = input_images["in1"], input_images["in2"]

        try:
            i_out = cv2.absdiff(i1, i2)
        except:
            print(f"input #1: shape={i1.shape}")
            print(f"input #2: shape={i2.shape}")
            raise

        return self._make_output_images(out=i_out)
