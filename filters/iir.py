from enum import Enum, auto, unique

import numpy as np

from .filter_base import FilterBase, TFilterInputImages, TFilterOutputImages


@unique
class IIRMaskType(Enum):
    BINARY = auto()
    GRAY = auto()


class IIR(FilterBase):
    """
    if mask:
        0     -- don't accumulate pixel
        != 0  -- accumulate

    if $\alpha$:
        0     -- result is a next frame
        != 0  -- result is a mixture of an accumulated background and a next frame
    """

    def __init__(self, input, alpha, mask=None, invert_mask=False, mask_type=IIRMaskType.BINARY):
        super().__init__({"in": input, "mask": mask}, ["out"])
        assert 0 <= alpha < 1
        assert mask_type in IIRMaskType

        self._alpha = alpha
        self._invert_mask = invert_mask
        self._mask_type = mask_type
        self._acc = None

    def _do_process_image(self, n: int, input_images: TFilterInputImages) -> TFilterOutputImages:
        i_in = input_images["in"]
        mask = input_images["mask"]

        if self._acc is None:
            #self._acc = np.zeros(i_in.shape, dtype=np.float64)
            self._acc = i_in.astype(np.float64)
        
        mix = self._alpha * self._acc  +  (1.0 - self._alpha) * i_in

        if (mask is not None) and (self._mask_type is IIRMaskType.BINARY):
            pixels_to_acc = (mask != 0) if not self._invert_mask else (mask == 0)
            self._acc[pixels_to_acc] = mix[pixels_to_acc]

        elif (mask is not None) and (self._mask_type is IIRMaskType.GRAY):
            pixels_to_acc = (mask) if (not self._invert_mask) else (255 - mask)
            pixels_to_acc = pixels_to_acc.astype(np.float64) / 255.0
            self._acc = (1.0 - pixels_to_acc) * self._acc + pixels_to_acc * mix

        else:
            self._acc = mix

        i_out = self._acc.astype(np.uint8)  # Always lays in the interval [0, 255]
        return self._make_output_images(out=i_out)
