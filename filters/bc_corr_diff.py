import cv2
import numpy as np
from scipy import stats

from .filter_base import FilterBase, TFilterInputs, TFilterOutputFrames


class BCCorrDiff(FilterBase):
    """
    Brightness and Contrast correction by means of 
        $$ I1' = a * I1 + b $$
    where $a$, and $b$ got from linear a least-squares regression on $(I2, I1)$.
    """

    def __init__(self, input):
        super().__init__({"in": input}, ["out"], input_buffer_min_len=2)

    def _do_process_inputs(self, n: int, inputs: TFilterInputs) -> TFilterOutputFrames:
        inpt = inputs["in"]

        i1 = inpt.get_frame(n-1).data
        i2 = inpt.get_frame(n).data

        i1_arr = i1.reshape(-1)
        i2_arr = i2.reshape(-1)
        regress = stats.linregress(i1_arr, i2_arr)

        a = regress.slope
        b = regress.intercept
        i1_prime = a * i1 + b

        #i_out = cv2.absdiff(i2, i1_prime.astype(np.uint8))
        i_out = np.minimum(255, np.abs(i2 - i1_prime)).astype(np.uint8)
        #i_out = np.abs(i2 - i1_prime)

        assert not np.any(i_out < 0), i_out < 0
        assert not np.any(i_out > 255)

        output_images = self._make_output_images(out=i_out)
        return self._output_images_2_output_frames(n, output_images)
