import cv2
import numpy as np

from .filter_base import List, FilterBase, TFilterInputImages, TFilterOutputImages, TFilterInputs


class FrameCapture(FilterBase):

    def __init__(self, input, n: int):
        super().__init__({"input": input}, ["out", "captured"])
        assert n >= 1, f"n={n}"
        self._n = n

    def _do_init_inputs(self, inputs: TFilterInputs):
        input = inputs["input"]
        self._captured_img = np.zeros(input.shape, dtype=np.uint8)

    def _do_process_image(self, n: int, input_images: TFilterInputImages) -> TFilterOutputImages:
        i_in = input_images["input"]

        if n == self._n:
            self._captured_img = i_in
        
        return self._make_output_images(out=i_in, captured=self._captured_img)
