import cv2

from .filter_base import FilterBase, TFilterInputs, TFilterOutputFrames


class SimpleDiff(FilterBase):

    def __init__(self, input):
        super().__init__({"in": input}, ["out"], input_buffer_min_len=2)

    def _do_process_inputs(self, n: int, inputs: TFilterInputs) -> TFilterOutputFrames:
        inpt = inputs["in"]

        i1 = inpt.get_frame(n-1).data
        i2 = inpt.get_frame(n).data
        i_out = cv2.absdiff(i2, i1)

        output_images = self._make_output_images(out=i_out)
        return self._output_images_2_output_frames(n, output_images)
