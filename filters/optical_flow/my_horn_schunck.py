import cv2
import numpy as np
from scipy.ndimage import convolve

from ..filter_base import FilterBase, TFilterInputs, TFilterOutputFrames


class MyHornSchunck(FilterBase):

    def __init__(self, input, magnitude_scale_factor=1.0):
        super().__init__({"input": input}, ["out_scaled", "out_normalized"], input_buffer_min_len=2)
        self.magnitude_scale_factor = magnitude_scale_factor
        self.λ = 0.1
        self.iterations = 5

    def _do_process_inputs(self, n: int, inputs: TFilterInputs) -> TFilterOutputFrames:
        inpt = inputs["input"]

        I_at_n = cv2.pyrDown(inpt.get_frame(n).data)
        I_at_n_1 = cv2.pyrDown(inpt.get_frame(n-1).data)

        I_x = self.get_I_x(I_at_n, I_at_n_1)
        I_y = self.get_I_y(I_at_n, I_at_n_1)
        I_t = self.get_I_t(I_at_n, I_at_n_1)

        u = np.zeros(I_at_n.shape, dtype=np.float64)
        v = np.zeros(I_at_n.shape, dtype=np.float64)

        for _ in range(self.iterations):
            u_mean = self.get_4adjacent_mean(u)
            v_mean = self.get_4adjacent_mean(v)
            
            α = (I_x * u_mean + I_y * v_mean + I_t) / (self.λ**2 + I_x**2 + I_y**2)
            u = u_mean - I_x * α
            v = v_mean - I_y * α
        
        mag, _ = cv2.cartToPolar(u, v)
        # TODO: limit values by 255

        mag_normalized = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
        mag_scaled = self.magnitude_scale_factor * mag

        output_images = self._make_output_images(
            out_scaled=cv2.pyrUp(mag_scaled.astype(np.uint8)),
            out_normalized=cv2.pyrUp(mag_normalized.astype(np.uint8)))
        return self._output_images_2_output_frames(n, output_images)

    @classmethod
    def get_4adjacent_mean(cls, arr):
        ker = np.array([
            [0, 1, 0],
            [1, 0, 1],
            [0, 1, 0]
        ])
        #return cv2.filter2D(arr, None, -1, ker)
        return convolve(arr, ker, mode="constant", cval=0.0, origin=(-1, -1))
        
    @classmethod
    def get_I_x(cls, I1, I2):
        return cls._get_derivative(I1, I2, [
            [
                [-1,  1],
                [-1,  1]
            ],
            [
                [-1,  1],
                [-1,  1]
            ]
        ])

    @classmethod
    def get_I_y(cls, I1, I2):
        return cls._get_derivative(I1, I2, [
            [
                [-1, -1],
                [ 1,  1]
            ],
            [
                [-1, -1],
                [ 1,  1]
            ]
        ])

    @classmethod
    def get_I_t(cls, I1, I2):
        return cls._get_derivative(I1, I2, [
            [
                [-1, -1],
                [-1, -1]
            ],
            [
                [ 1,  1],
                [ 1,  1]
            ]
        ])

    @classmethod
    def _get_derivative(cls, I1, I2, ker):
        conv = convolve(np.array([I1, I2]), np.array(ker), mode="constant", cval=0.0)
        return 0.25 * conv[0, :, :]