import cv2
import numpy as np
from .tuples import FrameData

class FrameDiffFilter:

    def __init__(self, gaussian_blur_size, bin_thresh, dilate_kernel_size, iir_alpha, dilate_iter=1):
        self._prev_gray = None
        self._background = None
        self._gaussian_blur_size = gaussian_blur_size
        self._thresh = bin_thresh
        self._dilate_kernel_size = dilate_kernel_size
        self._dilate_iter = dilate_iter
        self._alpha = iir_alpha

    def next_frame(self, frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        if self._gaussian_blur_size:
            gray = cv2.GaussianBlur(gray, (self._gaussian_blur_size, self._gaussian_blur_size), 0)

        if self._prev_gray is None:
            self._prev_gray = gray
        
        difference = cv2.absdiff(gray, self._prev_gray)

        mask_max_val = 255
        _, mask = cv2.threshold(difference, self._thresh, mask_max_val, cv2.THRESH_BINARY)

        if self._dilate_kernel_size:
            kernel = np.ones((self._dilate_kernel_size, self._dilate_kernel_size), np.uint8)
            mask = cv2.dilate(mask, kernel, iterations=self._dilate_iter)

        if self._background is None:
            self._background = frame

        norm_mask = np.repeat(mask[:, :, np.newaxis], 3, axis=2) / mask_max_val
        bckg_update = norm_mask * self._background + (1.0 - norm_mask) * frame
        self._background = (1.0 - self._alpha) * self._background + self._alpha * bckg_update

        properties = f'guass={self._gaussian_blur_size}, bin_th={self._thresh}'
        return (
            FrameData(f'Frame diff mask: {properties}', mask, cv2.COLOR_BGR2RGB),
            FrameData(f'Frame diff filter: {properties}', self._background.astype(np.uint8), cv2.COLOR_BGR2RGB)
        )