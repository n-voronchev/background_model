from __future__ import annotations  # [PEP 563]
from typing import List

import numpy as np

from .helpers import img_float_to_uint8


TImage = np.ndarray


class Frame:

    @classmethod
    def black_frame(cls, n: int, shape):
        img = np.zeros(shape, dtype=np.uint8)
        return cls(img, n)

    @classmethod
    def black_frame_ondemand_shape(cls, n: int):
        img = np.zeros((0,0), dtype=np.uint8)
        frame = cls(img, n)
        frame._ondemand_shape_value = 0
        return frame

    @classmethod
    def get_frame_from_ondemand(cls, ondemand_frame: Frame, shape):
        assert ondemand_frame._data.size == 0
        img = np.full(shape, ondemand_frame._ondemand_shape_value, dtype=np.uint8)
        return cls(img, ondemand_frame.n)

    """
    @classmethod
    def get_dummy(cls, n: int):
        dummy = cls(np.zeros((0,0), dtype=np.uint8), n)
        dummy.is_dummy = True
        return dummy

    @classmethod
    def convert_dummy_to_zeros(cls, dummy: Frame, shape):
        return cls(np.zeros(shape, dtype=dummy.dtype), dummy.n)
    """

    def __init__(self, data: TImage, n: int):
        #self.data = data.astype(np.float64)
        assert data.dtype == np.uint8
        self.n = n
        self._data = data
        self._ondemand_shape_value = None

    @property
    def data(self):
        if self._data.size == 0:
            raise Exception("On demand frame")
        return self._data

    """
    @property
    def data_uint8(self):
        # return img_float_to_uint8(self.data)
        #return np.minimum(255, np.abs(self.data)).astype(np.uint8)
        return self.data.astype(np.uint8)
    """

    """
    @property
    def data_uint8(self):
        # return img_float_to_uint8(self.data)
        #return np.minimum(255, np.abs(self.data)).astype(np.uint8)
        return self.data.astype(np.uint8)
    """
