from collections import namedtuple

FrameData = namedtuple('FrameData', ('title', 'frame', 'color_conv_code'))