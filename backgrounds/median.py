import cv2
import numpy as np
from .tuples import FrameData

class MedianFilter:

    def __init__(self, buffer_len, time_offset):
        self._k = buffer_len
        self._offset = time_offset
        self._buffer = None

    def next_frame(self, frame):
        if self._buffer is None:
            self._buffer = np.zeros(list(frame.shape) + [self._k], dtype=np.uint8)
            self._ki = 0

        self._buffer[:, :, :, self._ki].flat[:] = frame.flat[:]
        self._ki = (self._ki + 1) % self._k

        median = np.median(self._buffer, axis=3).astype(dtype=np.uint8)
        return FrameData('Median filter', median, cv2.COLOR_BGR2RGB)
