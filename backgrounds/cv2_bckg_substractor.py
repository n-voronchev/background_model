import cv2
import numpy as np
from .tuples import FrameData

class Cv2BckgSubstractor:

    def __init__(self, substractor, substractor_name, init_frame, iir_alpha):
        self._alpha = iir_alpha
        self._background = init_frame.astype(np.float64)
        self._substractor = substractor
        self._substractor_name = substractor_name

        self._substractor.apply(init_frame)

        self.erode_kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
        self.dilate_kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 7))

    def next_frame(self, in_frame):
        mask = self._substractor.apply(in_frame)

        _, mask = cv2.threshold(mask, 244, 255, cv2.THRESH_BINARY)
        cv2.erode(mask, self.erode_kernel, mask, iterations=2)
        cv2.dilate(mask, self.dilate_kernel, mask, iterations=2)

        max_val = 255
        norm_mask = np.repeat(mask[:, :, np.newaxis], 3, axis=2) / max_val
        norm_inv_mask = 1.0 - norm_mask

        bckg_update_frame = norm_mask * self._background + norm_inv_mask * in_frame
        self._background = (1.0 - self._alpha) * self._background + self._alpha * bckg_update_frame

        return (
            FrameData(f'{self._substractor_name} frame', self._background.astype(np.uint8), cv2.COLOR_BGR2RGB),
            FrameData(f'{self._substractor_name} mask', mask, cv2.COLOR_BGR2RGB)
        )
