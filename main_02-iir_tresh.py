import sys

import scipy
import numpy as np

from filters import *  #FileReader, Gray, SimpleDiff, BCCorrDiff, InputsDiff, Threshold, MixByMask, GaussianBlur, IIR, Histogram, Max
from application import Application, Column, TAppPipelineReturn


class MyApplication(Application):
    
    def __init__(self, video_src, **kwargs):
        super().__init__(**kwargs)
        self.video_src = video_src

    def pipeline(self) -> TAppPipelineReturn:
        file_reader = FileReader(self.video_src)
        
        src_as_gray = Gray(file_reader.out)
        
        blured_gray_src = GaussianBlur(  # Reduce nose in frame-difference
            src_as_gray.out,
            size=15
        )
        
        simple_diff = SimpleDiff(blured_gray_src.out)
        
        bc_corr_diff = BCCorrDiff(blured_gray_src.out)
        
        bc_corr_diff_iir = IIR(  # Suppress residual noise in frame-differense (which goes through Gaussian blur)
            bc_corr_diff.out,
            alpha=0.3,
        )

        threshold_framediff = Threshold(
            bc_corr_diff_iir.out,
            #thresh=lambda i: 0.5 * np.mean(i[i > 0]),
            thresh=1, # ???: Magic value 
            erode_size=5,  # ??? 1% from max(w,h)
            erode_iter=3,
            dilate_size=5,
            dilate_iter=4
        )
        threshold_framediff_check = MixByMask(
            img0=src_as_gray.out,
            img1=0,
            mask=threshold_framediff.out
        )

        threshold_framediff_iir = IIR(
            threshold_framediff.out,
            alpha=0.9,
        )
        threshold_framediff_iir_check = MixByMask(
            img0=src_as_gray.out,
            img1=0,
            mask=threshold_framediff_iir.out
        )

        bckg_model_0 = IIR(
            input=src_as_gray.out,
            alpha=0.95,
            mask=threshold_framediff.out,
            invert_mask=True
        )
        moving_objects_0 = InputsDiff(
            bckg_model_0.out,
            src_as_gray.out
        )

        bckg_model_1 = IIR(
            input=src_as_gray.out,
            alpha=0.95,
            mask=threshold_framediff_iir.out,
            invert_mask=True
        )
        moving_objects_1 = InputsDiff(
            bckg_model_1.out,
            src_as_gray.out
        )

        outs_to_be_displayed = {
            "file_reader": file_reader.out,
            "blured_gray_src": blured_gray_src.out,
            "simple_diff": simple_diff.out,
            "bc_corr_diff": bc_corr_diff.out,
            "bc_corr_diff_iir": bc_corr_diff_iir.out,
            "threshold_framediff": threshold_framediff.out,
            "threshold_framediff_check": threshold_framediff_check.out,
            "bckg_model_0": bckg_model_0.out,
            "moving_objects_0": moving_objects_0.out,
            "bckg_model_1": bckg_model_1.out,
            "moving_objects_1": moving_objects_1.out,
            "threshold_framediff_iir": threshold_framediff_iir.out,
            "threshold_framediff_iir_check": threshold_framediff_iir_check.out,
        }

        return outs_to_be_displayed

    def columns(self):
        return [
            Column("source",
                [
                    "file_reader",
                    "blured_gray_src",
                ]),
            Column("simple_diff & bc_corr_diff",
                [
                    "bc_corr_diff_iir",
                    "simple_diff",
                ]),
            Column("thresholded bc_corr_diff",
                [
                    "threshold_framediff",
                    "threshold_framediff_check",
                ]),
            Column("",
                [
                    "threshold_framediff_iir",
                    "threshold_framediff_iir_check",
                ]),
            Column("backgound and moving objects",
                [
                    "bckg_model_0",
                    "moving_objects_0",
                ]),
            Column("backgound and moving objects",
                [
                    "bckg_model_1",
                    "moving_objects_1",
                ]),
        ]


video_src = sys.argv[1] if len(sys.argv) == 2 else 'axis800x600_oh20.mp4'
app = MyApplication(video_src, w=2300, h=1150)
app.run()