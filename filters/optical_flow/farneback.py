import cv2
import numpy as np

from ..filter_base import FilterBase, TFilterInputs, TFilterOutputFrames


class Farneback(FilterBase):

    def __init__(self, input, magnitude_scale_factor=1.0):
        super().__init__({"input": input}, ["out_scaled", "out_normalized"], input_buffer_min_len=2)
        self.magnitude_scale_factor = magnitude_scale_factor

    def _do_process_inputs(self, n: int, inputs: TFilterInputs) -> TFilterOutputFrames:
        inpt = inputs["input"]

        I_at_n = cv2.pyrDown(inpt.get_frame(n).data)
        I_at_n_1 = cv2.pyrDown(inpt.get_frame(n-1).data)

        flow = cv2.calcOpticalFlowFarneback(I_at_n_1, I_at_n, None, pyr_scale=0.5, levels=3, winsize=25, iterations=3, poly_n=5, poly_sigma=1.1, flags=0)
        mag, _ = cv2.cartToPolar(flow[:,:,0], flow[:,:,1])
        # TODO: limit values by 255

        mag_normalized = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
        mag_scaled = self.magnitude_scale_factor * mag

        output_images = self._make_output_images(
            out_scaled=cv2.pyrUp(mag_scaled.astype(np.uint8)),
            out_normalized=cv2.pyrUp(mag_normalized.astype(np.uint8)))
        return self._output_images_2_output_frames(n, output_images)