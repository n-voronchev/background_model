import sys

import scipy
import numpy as np

from filters import *  #FileReader, Gray, SimpleDiff, BCCorrDiff, InputsDiff, Threshold, MixByMask, GaussianBlur, IIR, Histogram, Max
from application import Application, Column, TAppPipelineReturn


class MyApplication(Application):
    
    def __init__(self, video_src, **kwargs):
        super().__init__(**kwargs)
        self.video_src = video_src

    def pipeline(self) -> TAppPipelineReturn:
        file_reader = FileReader(self.video_src)
        
        src_as_gray = Gray(file_reader.out)
        
        blured_gray_src = GaussianBlur(  # Reduce nose in frame-difference
            src_as_gray.out,
            size=15
        )

        blured_gray_src_iir = IIR(  # Suppress residual noise in frame-differense (which goes through Gaussian blur)
            blured_gray_src.out,
            alpha=0.3,  # 0.3
        )

        farneback = Farneback(
            blured_gray_src_iir.out,
            magnitude_scale_factor=5
        )
        farneback_thresh = Threshold(
            farneback.out_scaled,
            #thresh=lambda i: 0.5 * np.mean(i[i > 0]),
            thresh=1,
            erode_size=5,
            erode_iter=3,
            dilate_size=7,
            dilate_iter=5
        )
        bckg_color = IIR(
            input=file_reader.out,
            alpha=0.98,
            mask=farneback_thresh.out,
            mask_type=IIRMaskType.BINARY,
            invert_mask=True,
        )
        bckg_gray = Gray(bckg_color.out)

        """
        bckg_frame_diff = InputsDiff(
            bckg_gray.out,
            src_as_gray.out,
        )
        """
        bckg_frame_diff_color = InputsDiff(
            bckg_color.out,
            file_reader.out,
        )
        bckg_frame_diff = Gray(bckg_frame_diff_color.out)

        bckg_frame_diff_thresh = Threshold(
            bckg_frame_diff.out,
            thresh=10,
            erode_size=10,
            erode_iter=3,
            dilate_size=10,
            dilate_iter=5
        )

        motion = Max(
            farneback_thresh.out,
            bckg_frame_diff_thresh.out
        )
        x = InputsDiff(
            motion.out,
            farneback_thresh.out
        )

        tracking = SimpleTracker(
            file_reader.out,
            motion.out,
        )

        outs_to_be_displayed = {
            "file_reader": file_reader.out,
            "blured_gray_src_iir": blured_gray_src_iir.out,
            "farneback_thresh": farneback_thresh.out,
            "farneback_normalized": farneback.out_normalized,
            "bckg_color": bckg_color.out,
            "bckg_frame_diff": bckg_frame_diff.out,
            "bckg_frame_diff_thresh": bckg_frame_diff_thresh.out,
            "motion": motion.out,
            "x": x.out,
            "tracking": tracking.out,
        }

        return outs_to_be_displayed

    def columns(self):
        return [
            Column("source",
                [
                    "file_reader",
                    "blured_gray_src_iir",
                ]),
            Column("",
                [
                    "bckg_color",
                    "farneback_thresh",
                    "farneback_normalized",
                ]),
            Column("",
                [
                    "bckg_frame_diff",
                    "bckg_frame_diff_thresh",
                ]),
            Column("",
                [
                    "x",
                    "motion",
                ]),
            Column("",
                [
                    "tracking",
                ]
            )
        ]

video_src = sys.argv[1] if len(sys.argv) == 2 else 'axis800x600_oh20.mp4'
app = MyApplication(video_src, w=2300, h=1150)
app.run()