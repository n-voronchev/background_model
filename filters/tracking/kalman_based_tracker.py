import numpy as np
import cv2

from ..filter_base import FilterBase, TFilterInputs, TFilterOutputFrames

class TrackingObject:

    def __init__(self, mask_object, dt=1, measurementSigma=0.5, processNoiseSigma=0.02):
        """
            dt:                Шаг по времени
            measurementSigma:  Ошибка измерения \sigma_w (среднеквадратичное отклонение)
            processNoiseSigma: <Погрешность модели> = \sigma_v (среднеквадратичное отклонение)
        """
        #self.dt = dt
        self.mask_object = mask_object
        self.kalman = self._make_kalman(mask_object.track_point, dt, measurementSigma, processNoiseSigma)

    def update(self):
        pass

    def predict(self):
        pass

    @classmethod
    def _make_kalman(cls, initial_track_point, dt, measurementSigma, processNoiseSigma):
        """
            x_k  =  F x_{k-1}  +  B u_k  +  w_k
            z_k  =  H x_k  +  v_k

            x_k:  { x, x', x'', y, y', y'' }

            https://habr.com/ru/company/singularis/blog/516798/
            https://docs.opencv.org/master/dd/d6a/classcv_1_1KalmanFilter.html
            https://en.wikipedia.org/wiki/Kalman_filter
        """
        kalman = cv2.KalmanFilter(2*3, 2)

        # Matrix 'F'
        kalman.transitionMatrix = np.array(
            [[       1,      dt, dt**2/2,       0,       0,       0],
             [       0,       1,      dt,       0,       0,       0],
             [       0,       0,       1,       0,       0,       0],
             [       0,       0,       0,       1,      dt, dt**2/2],
             [       0,       0,       0,       0,       1,      dt],
             [       0,       0,       0,       0,       0,       1]], np.float32)

        # Matrix 'H'
        kalman.measurementMatrix = np.array(
            [[ 1, 0, 0, 0, 0, 0 ],
             [ 0, 0, 0, 1, 0, 0 ]], np.float32)
       
        # Matrix 'Q'
        kalman.processNoiseCov = np.array(
            [[ dt**4/4, dt**3/2, dt**2/2,       0,       0,       0 ],
             [ dt**3/2,   dt**2,      dt,       0,       0,       0 ],
             [ dt**2/2,      dt,       1,       0,       0,       0 ],
             [       0,       0,       0, dt**4/4, dt**3/2, dt**2/2 ],
             [       0,       0,       0, dt**3/2,   dt**2,      dt ],
             [       0,       0,       0, dt**2/2,      dt,       1 ]], np.float32) * (processNoiseSigma**2)

        # Matrix 'R'
        kalman.measurementNoiseCov = np.array(
            [[ 1, 0 ],
             [ 0, 1 ]], np.float32) * (measurementSigma**2)
        
        tp_x, tp_y = initial_track_point
        
        kalman.statePre = np.array(
            [[tp_x],
             [tp_y],
             [0],
             [0]], np.float32)
        
        kalman.statePost = np.array(
            [[tp_x],
             [tp_y],
             [0],
             [0]], np.float32)

        return kalman

