import cv2
import numpy as np
from scipy.ndimage import convolve

from ..filter_base import FilterBase, TFilterInputs, TFilterOutputFrames


class MyLucasKanade(FilterBase):

    def __init__(self, input, sobel_ksize=1, lk_ksize=3):
        """
        Assume input is blured to reduce noise.
        """
        super().__init__({"in": input}, ["out"], input_buffer_min_len=2)
        self.sobel_ksize = sobel_ksize
        self.lk_ksize = lk_ksize
        self.h_t = 1
        # TODO: self.h_xy = 1

        self.lk_ker = np.ones((lk_ksize, lk_ksize))

    def _do_process_inputs(self, n: int, inputs: TFilterInputs) -> TFilterOutputFrames:
        inpt = inputs["in"]

        I_at_n = cv2.pyrDown(inpt.get_frame(n).data)
        I_at_n_1 = cv2.pyrDown(inpt.get_frame(n-1).data)

        I_x = cv2.Sobel(I_at_n, cv2.CV_64F, 1, 0, ksize=self.sobel_ksize)  # multimplcate due to kernel size?
        I_y = cv2.Sobel(I_at_n, cv2.CV_64F, 0, 1, ksize=self.sobel_ksize)  # multimplcate due to kernel size?
        I_t = (I_at_n - I_at_n_1).astype(np.float64) / self.h_t

        #grad_L2 = np.sqrt(I_x * I_x + I_y * I_y)
        #g_x = I_x / grad_L
        #g_y = I_y / grad_L2
        #b = - I_t / grad_L2
        
        g_x = I_x
        g_y = I_y
        b = - I_t

        def lk_conv(arr):
            origin_shift = self.lk_ksize // 2
            return convolve(arr, self.lk_ker, mode="constant", cval=0.0, origin=(-origin_shift, -origin_shift))

        GT_G_11 = lk_conv(g_x**2)
        GT_G_12 = lk_conv(g_x * g_y)
        GT_G_21 = GT_G_12
        GT_G_22 = lk_conv(g_y**2)

        GT_B_1 = lk_conv(g_x * b)
        GT_B_2 = lk_conv(g_y * b)

        GT_G_det = (GT_G_11 * GT_G_22 - GT_G_12 * GT_G_21).astype(np.float64)
        u_1 = (  GT_G_22 * GT_B_1 - GT_G_21 * GT_B_2) / GT_G_det
        u_2 = (- GT_G_12 * GT_B_1 + GT_G_11 + GT_B_2) / GT_G_det

        i_out = np.sqrt(u_1**2 + u_2**2)

        i_out = i_out.astype(np.uint8)  # Always lays in the interval [0, 255]

        output_images = self._make_output_images(out=cv2.pyrUp(i_out))
        return self._output_images_2_output_frames(n, output_images)