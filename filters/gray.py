import cv2
import numpy as np

from .filter_base import FilterBase, TFilterInputImages, TFilterOutputImages


class Gray(FilterBase):

    def __init__(self, input):
        super().__init__({"in": input}, ["out"])

    def _do_process_image(self, n: int, input_images: TFilterInputImages) -> TFilterOutputImages:
        i_in = input_images["in"]

        #.astype(np.uint8)
        i_out = cv2.cvtColor(i_in, cv2.COLOR_BGR2GRAY)
        
        return self._make_output_images(out=i_out)
