import cv2
from .cv2_bckg_substractor import Cv2BckgSubstractor

class KNNFilter(Cv2BckgSubstractor):

    def __init__(self, init_frame, iir_alpha):
        super().__init__(cv2.createBackgroundSubtractorKNN(), 'KNN', init_frame, iir_alpha)
