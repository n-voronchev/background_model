import cv2
import numpy as np

from .filter_base import FilterBase, TFilterInputImages, TFilterOutputImages


class Max(FilterBase):

    def __init__(self, in1, in2):
        super().__init__({"in1": in1, "in2": in2}, ["out"])

    def _do_process_image(self, n: int, input_images: TFilterInputImages) -> TFilterOutputImages:
        i1, i2 = input_images["in1"], input_images["in2"]

        i_out = np.copy(i2)

        idx = i1 > i2
        i_out[idx] = i1[idx]

        return self._make_output_images(out=i_out)
