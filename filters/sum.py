import cv2
import numpy as np

from .filter_base import FilterBase, TFilterInputImages, TFilterOutputImages


class Sum(FilterBase):
    """
    """

    def __init__(self, in1, in2, eta=1):
        super().__init__({"in1": in1, "in2": in2}, ["out"])
        self._eta = eta

    def _do_process_image(self, n: int, input_images: TFilterInputImages) -> TFilterOutputImages:
        in1 = input_images["in1"].astype(np.float64)
        in2 = input_images["in2"].astype(np.float64)

        out_float = self._eta * (in1 + in2) / 2.0
        out_float_limited = np.minimum(255, out_float)
        out_uint8 = out_float_limited.astype(np.uint8)
        
        return self._make_output_images(out=out_uint8)
